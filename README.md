# ProxyGenerator-bot
Bot for ProxyGenerator // ProxyGenerator Bot Telegram
Бот для генерации прокси Telegram, написан на библиотеке aiogram

Для корректной работы бота установите самую последнюю версию python.

```
ProxyGenerator бот телеграм, с генерацией прокси, и ссылки на админа и ча
```
### Установка:
```sh

cd ProxyGenerator-bot

pip install -r requirements.txt
```
### Настройка(config.py):

```python
token = "" # токен от вашего бота в телеграме (взять тут t.me/botfather)
source = "https://gimmeproxy.com/api/getProxy?curl=true&protocol=http&supportsHttps=true" # не трогайте
start_photo = "" # стартовое фото, либо ссылку либо путь к директори с фото 
chat_link = "codwiz" # ссылку на чат
admin_user = "codwiz" # username админк

```

### Запускаем
```sh
python app.py
```
