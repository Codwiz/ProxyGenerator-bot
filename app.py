from aiogram import types, Bot, Dispatcher
from requests import get
from config import token, source, start_photo, chat_link, admin_user 
from keyboards import main
import asyncio

bot = Bot(token = token,  parse_mode = 'HTML')
dp = Dispatcher(bot)
loop = asyncio.get_event_loop();


@dp.message_handler(commands=['start'])
async def cmd_start(m: types.Message):
    await bot.send_photo(m.from_user.id, start_photo, caption="<b>Добро пожаловать!</b>", reply_markup=main)

@dp.message_handler(text='🌩 Прокси')
async def generate_proxy(m: types.Message):
    proxy_data = get(source).text

    await m.answer(f"🌄 <b>Прокси сгенерирован!</b>\n\n"
                   f"{proxy_data}")

@dp.message_handler(text='🗽 Информация')
async def get_info(m: types.Message):
    await m.answer("⛱ <b>Информация:</b>\n\n"
                   f"Админ: {admin_user}\n"
                   f"Чат: {chat_link}\n", disable_web_page_preview=True)

@dp.message_handler(text='🗯 Наш чат')
async def get_chat_info(m: types.Message):
    await m.answer("🚁 <b>Наш чат:</b>\n\n"
                   f"{chat_link}", disable_web_page_preview=True)


loop.create_task(dp.start_polling())
loop.run_forever()